require_relative 'api_replication'

class IssueReplication < APIReplication
  def initialize
    @log_file = "./logs/issue_replication.log"
    @log_table_header = "| Issue ID | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|----------|-------------|---------------|----------|------------------|\n"
    @replication_status_path = "/api/v4/issues/"
  end

  def upload_file
    @project = create_project
    endpoint = "api/v4/projects/#{@project['id']}/issues"

    url = URI("#{Config.primary_site}/#{endpoint}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = url.scheme == 'https'

    request = Net::HTTP::Post.new(url)
    request["PRIVATE-TOKEN"] = Config.access_token
    request["Content-Type"] = "application/json"
    request.body = JSON.dump(
      {
        "title": random_words(rand(2..4)),
        "description": random_words(rand(5..8)),
      }
    )

    post_response = JSON.parse(send_request(http, request).body)
    post_response['web_url']
  end
end
