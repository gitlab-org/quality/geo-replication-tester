require_relative 'api_replication'

class RepositoryReplication < APIReplication
  def initialize
    @log_file = "./logs/repository_replication.log"
    @log_table_header = "| Project ID | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|------------|-------------|---------------|----------|------------------|\n"
  end

  def upload_file
    @project = create_project(true)
    @item_id = @project['id']
    @project["http_url_to_repo"]
  end

  def replication_finished?
    HTTParty.get(URI.parse("#{Config.secondary_site}/api/v4/projects/#{@item_id}/repository/tree"), headers:
    {
      "Authorization" => "Bearer #{Config.access_token}"
    })
  end
end
