class ReplicationType
  require "uri"
  require "net/http"
  require 'httparty'
  require 'json'
  require 'down'

  require 'config'
  require 'replication_logger'
  require 'replication_result'

  attr_accessor :fastest_replication, :slowest_replication, :average_replication, :replication_count, :log_table_header, :replication_status_path, :log_file, :limit, :result, :project

  @logger = ""

  def start_replication
    @fastest_replication = 999
    @slowest_replication = 0
    @average_replication = 0
    @replication_count = 0

    @logger = ReplicationLogger.new(@log_file, @log_table_header)
    replicate
    @logger.end_log(@fastest_replication, @slowest_replication, (@average_replication / @replication_count).round(2))
  end

  def replicate
    threads = []
    Config.set_end_time

    thread_count = 0
    @limit = 5 if @limit.nil?

    until Time.now > Config.end_time
      if thread_count < @limit
        threads << Thread.new do
          thread_count += 1
          @result = ReplicationResult.new
          @result.file_path = upload_file
          @result.set_start_time

          finished, code = replication_status? until finished

          @result.finish(code)
          @fastest_replication = @result.duration unless @fastest_replication < @result.duration
          @slowest_replication = @result.duration unless @slowest_replication > @result.duration
          @average_replication += @result.duration
          @replication_count += 1

          @logger.add_result("| #{@result.file_path} | #{@result.start} | #{@result.end} | #{@result.duration}s | #{@result.status} |")
          thread_count -= 1
        end

       threads.each(&:join)
      end
    end
  end

  def replication_status?
    return false, 404 if Time.now > result.max_runtime

    begin
      replication_finished?
    rescue SocketError
      return false, 404
    end
  end

  def random_words(number_of_words)
    word = Array.new(number_of_words) { (1...(rand(3..10))).map { ('a'..'z').to_a[rand(1..26)] }.join }.join(" ")
    word.strip! || word
  end

  def create_project(create_readme = false)
    endpoint = "api/v4/projects"
    visibilities = %w[private internal public]

    url = URI("#{Config.primary_site}/#{endpoint}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = url.scheme == 'https'

    request = Net::HTTP::Post.new(url)
    request["PRIVATE-TOKEN"] = Config.access_token
    request["Content-Type"] = "application/json"
    request.body = JSON.dump(
      {
        "name": random_words(rand(2..4)),
        "description": random_words(rand(5..8)),
        "visibility": visibilities.sample,
        "initialize_with_readme": create_readme
      }
    )

    Config.project_count += 1
    JSON.parse(send_request(http, request).body)
  end

  def send_request(http, request)
    request_sent = false
    request_count = 0

    until request_sent || request_count == 5
      begin
        response = http.request(request)
        request_sent = true
      rescue SocketError
        request_count += 1
      end
    end

    return response
  end

  def post_graphql_request(endpoint, query)
    url = URI("#{Config.secondary_site}/api/v4/#{endpoint}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = url.scheme == 'https'

    request = Net::HTTP::Post.new(url)
    request["PRIVATE-TOKEN"] = Config.access_token
    request["Content-Type"] = "application/json"
    request.body = query

    JSON.parse(send_request(http, request).body)
  end
end
