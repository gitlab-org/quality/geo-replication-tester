require_relative 'replication_type'

class ContainerReplication < ReplicationType
  @image_name = ""

  def initialize
    @limit = 1
    @log_file = "./logs/container_replication.log"
    @log_table_header = "| Project ID | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|------------|-------------|---------------|----------|------------------|\n"
  end

  def upload_file
    @project = create_project
    @image_name = random_words(1)

    run_docker_command("docker build -t #{@project['container_registry_image_prefix']}:#{@image_name} ./files/docker")
    run_docker_command("docker push #{@project['container_registry_image_prefix']}:#{@image_name}")

    @project["http_url_to_repo"]
  end

  def get_request(endpoint)
    JSON.parse(HTTParty.get(URI.parse("#{Config.secondary_site}/api/v4/#{endpoint}"), headers:
    {
      "Authorization" => "Bearer #{Config.access_token}"
    }).body)
  end

  def run_docker_command(command)
    _, docker_err, docker_status = Open3.capture3(command)

    unless docker_status.success?
      puts docker_err
      exit(1)
    end
  end

  def replication_finished?
    container_repository_id = get_request("projects/#{@project['id']}/registry/repositories")[0]["id"]
    container_repository_gql = post_graphql_request('geo/node_proxy/2/graphql', '{"query":"{geoNode {containerRepositoryRegistries(ids: \"gid://gitlab/Geo::ContainerRepositoryRegistry/' + container_repository_id.to_s + '\") {nodes {id state}}}}"}')
    container_repository_attrs = container_repository_gql.dig('data', 'geoNode', 'containerRepositoryRegistries', 'nodes').find { |cr| cr['id'].include?("ContainerRepositoryRegistry/#{container_repository_id}") }

    return false, 404 if container_repository_attrs.nil?
    return true, 200 if container_repository_attrs["state"]=="SYNCED"
    return false, 404
  end
end
