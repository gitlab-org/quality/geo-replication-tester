require_relative 'api_replication'

class SnippetReplication < ReplicationType
  def initialize
    @log_file = "./logs/snippet_replication.log"
    @log_table_header = "| Snippet ID | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|------------|-------------|---------------|----------|------------------|\n"
    @replication_status_path = "/api/v4/snippets/"
  end

  def upload_file
    endpoint = "api/v4/snippets"
    visibilities = %w[private internal public]
    file_extensions = %w[rb txt py md pl sh php java js html cs]

    url = URI("#{Config.primary_site}/#{endpoint}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = url.scheme == 'https'

    request = Net::HTTP::Post.new(url)
    request["PRIVATE-TOKEN"] = Config.access_token
    request["Content-Type"] = "application/json"
    request.body = JSON.dump(
      {
        "title": random_words(rand(3..6)),
        "description": random_words(rand(5..8)),
        "visibility": visibilities.sample,
        "files": [{
          "content": random_words(rand(1..1000)),
          "file_path": "#{random_words(1)}.#{file_extensions.sample}"
        }]
      }
    )

    post_response = JSON.parse(send_request(http, request).body)
    @item_id = post_response['id']
    post_response["web_url"]
  end

  def replication_finished?
    puts "Checking #{Config.secondary_site}#{replication_status_path}#{@item_id}" if Config.verbose
    snippet_gql = post_graphql_request('geo/node_proxy/2/graphql', '{"query":"{geoNode {snippetRepositoryRegistries(ids: \"gid://gitlab/Geo::SnippetRepositoryRegistry/' + @item_id.to_s + '\") {nodes {id state}}}}"}')
    snippet_attrs = snippet_gql.dig('data', 'geoNode', 'snippetRepositoryRegistries', 'nodes').find { |cr| cr['id'].include?("SnippetRepositoryRegistry/#{@item_id}") }

    return false, 404 if snippet_attrs.nil?
    return true, 200 if snippet_attrs["state"]=="SYNCED"
    return false, 404
  end
end
