require_relative 'api_replication'

class ImageReplication < APIReplication
  def initialize
    @log_file = "./logs/image_replication.log"
    @log_table_header = "| Image URL | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|-----------|-------------|---------------|----------|------------------|\n"
  end

  def upload_file
    @project = create_project
    endpoint = "api/v4/projects/#{@project['id']}/uploads"
    image_path = Down.download("https://picsum.photos/800")

    url = URI("#{Config.primary_site}/#{endpoint}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = url.scheme == 'https'

    request = Net::HTTP::Post.new(url)
    request["PRIVATE-TOKEN"] = Config.access_token
    form_data = [['file', File.open(image_path)]]
    request.set_form form_data, 'multipart/form-data'

    post_response = JSON.parse(send_request(http, request).body)
    @item_id = post_response['full_path']
  end
end
