require_relative 'replication_type'

class LFSReplication < ReplicationType
  def initialize
    @limit = 1
    @log_file = "./logs/lfs_replication.log"
    @log_table_header = "| Project Name | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|--------------|-------------|---------------|----------|------------------|\n"
  end

  def upload_file
    @project = create_project
    @repo_name = @project['path']

    FileUtils.rm_rf("./files/repos/tmp")
    FileUtils.copy_entry("./files/repos/lfs", "./files/repos/tmp")
    Down.download("https://picsum.photos/1024", destination: "./files/repos/tmp/#{@repo_name}.jpg")

    run_git_command("git add #{@repo_name}.jpg && git commit -m 'Add image #{@repo_name}.jpg'")
    run_git_command("git push --set-upstream #{@project['ssh_url_to_repo']} main")

    @project['http_url_to_repo']
  end

  def run_git_command(command)
    _, git_err, git_status = Open3.capture3(command, :chdir=>"./files/repos/tmp/")

    unless git_status.success?
      puts git_err
      exit(1)
    end
  end

  def replication_finished?
    puts "Checking #{Config.secondary_site}/api/v4/projects/#{@project['id']}/repository/files/#{@repo_name}.jpg?ref=main" if Config.verbose
    lfs_object = HTTParty.get(URI.parse("#{Config.secondary_site}/api/v4/projects/#{@project['id']}/repository/files/#{@repo_name}.jpg?ref=main"), headers:
    {
      "Authorization" => "Bearer #{Config.access_token}"
    })

    return lfs_object.code != 404, lfs_object.code
  end
end
