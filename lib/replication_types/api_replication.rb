require_relative 'replication_type'

class APIReplication < ReplicationType

  attr_accessor :item_id

  def replication_finished?
    puts "Checking #{Config.secondary_site}#{replication_status_path}#{@item_id}" if Config.verbose
    code = HTTParty.get(URI.parse("#{Config.secondary_site}#{replication_status_path}#{@item_id}"), headers:
    {
      "Authorization" => "Bearer #{Config.access_token}"
    }).code

    return  code != 404, code
  end
end
