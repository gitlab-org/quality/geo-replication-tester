require_relative 'replication_type'

class PackageReplication < ReplicationType
  def initialize
    @limit = 1
    @log_file = "./logs/package_replication.log"
    @log_table_header = "| Project Name | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|--------------|-------------|---------------|----------|------------------|\n"
  end

  def upload_file
    @project = create_project

    _, publish_err, publish_status = Open3.capture3("TWINE_PASSWORD=#{Config.access_token} TWINE_USERNAME=root python3 -m twine upload --repository-url #{Config.primary_site}/api/v4/projects/#{@project['id']}/packages/pypi ./files/packages/dist/*")

    unless publish_status.success?
      puts publish_err
      exit(1)
    end

    @project['http_url_to_repo']
  end

  def replication_finished?
    resposne = HTTParty.get(URI.parse("#{Config.secondary_site}/api/v4/projects/#{@project['id']}/packages"), headers:
    {
      "Authorization" => "Bearer #{Config.access_token}"
    })

    return true, resposne.code if resposne.length() == 1
    return false, 404
  end
end
