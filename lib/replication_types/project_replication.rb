require_relative 'api_replication'

class ProjectReplication < APIReplication
  def initialize
    @log_file = "./logs/project_replication.log"
    @log_table_header = "| Project ID | Uploaded At | Replicated At | Duration | HTTP Status Code |\n" \
                        "|------------|-------------|---------------|----------|------------------|\n"
    @replication_status_path = "/api/v4/projects/"
  end

  def upload_file
    @project = create_project
    @item_id = @project['id']
    @project["http_url_to_repo"]
  end
end
