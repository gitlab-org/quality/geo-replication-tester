class ReplicationResult
  attr_accessor :file_path, :status
  attr_reader :start_time, :end_time, :max_runtime

  require "active_support/isolated_execution_state"
  require 'active_support/core_ext/numeric/time'

  def initialize
    @max_runtime = 5.minutes.from_now
  end

  def set_start_time
    @start_time = Time.now
  end

  def finish(status)
    @status = status
    @end_time = Time.now
  end

  def start
    "#{@start_time.hour}:#{@start_time.min}:#{@start_time.sec}"
  end

  def end
    "#{@end_time.hour}:#{@end_time.min}:#{@end_time.sec}"
  end

  def duration
    (@end_time.to_time.round(2) - @start_time.to_time.round(2)).abs
  end
end
