class ReplicationLogger
  require "active_support/isolated_execution_state"
  require 'active_support/core_ext/numeric/time'

  require 'config'

  @log_file = ""
  @log_table_header = ""

  def initialize(log_file, log_table_header)
    @log_file = log_file
    @log_table_header = log_table_header

    log_create
  end

  def log_create
    log_header = "\nPrimary Site: #{Config.primary_site}\n"\
                 "Secondary Site: #{Config.secondary_site}\n"\
                 "Sending requests for #{Config.runtime} second(s)\n\n"\
                 "#{@log_table_header}"

    File.write(@log_file, log_header)
    puts log_header if Config.verbose
  end

  def add_result(result)
    File.write(@log_file, "#{result}\n", mode: "a")
    puts result if Config.verbose
  end

  def end_log(fastest_replication, slowest_replication, average_replication)
    log_sumary = "\nFastest Replication: #{fastest_replication}s\n"\
                 "Slowest Replication: #{slowest_replication}s\n"\
                 "Average Replication: #{average_replication}s\n"

    File.write(@log_file, log_sumary, mode: "a")
    puts log_sumary if Config.verbose
  end
end
