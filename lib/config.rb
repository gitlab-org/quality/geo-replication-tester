module Config
  extend self

  require "active_support/isolated_execution_state"
  require 'active_support/core_ext/numeric/time'

  attr_accessor :project_count
  attr_reader :primary_site, :secondary_site, :access_token, :loop_delay, :runtime, :end_time, :verbose

  def init(config, runtime, verbose, project_count)
    @primary_site = config['primary_site']
    @secondary_site = config['secondary_site']
    @access_token = config['access_token']
    @loop_delay = config['loop_delay']
    @runtime = runtime
    @verbose = verbose
    @project_count = project_count
  end

  def set_end_time
    @end_time = @runtime.seconds.from_now
  end

  def primary_registry
    @primary_site.sub(/^https?\:\/\/(www.)?/,'https://registry.')
  end

  def secondary_registry
    @secondary_site.sub(/^https?\:\/\/(www.)?/,'https://registry.')
  end
end
