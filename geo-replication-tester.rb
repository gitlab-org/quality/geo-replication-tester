#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('./lib', __dir__)

require 'optimist'
require 'yaml'
require 'require_all'
require 'open3'

require 'config'
require_all './lib/replication_types'

opts = Optimist.options do
  opt :config_file, "Relative path to a .yml config file to load", type: :string, short: "-c"
  opt :run_for_duration, "Number of seconds to run for", type: :integer, default: 60, short: "-r"
  opt :verbose, "Print logs to stdout", type: :flag, default: false, short: "-v"
  opt :project_count, "Current amount of projects", type: :integer, default: 1, short: "-p"
  opt :run_all, "Run all replication tests. Some tests require extra set up.", type: :flag, default: false ,short: "-a"
end

Config.init(YAML.load_file(opts[:config_file]), opts[:run_for_duration], opts[:verbose], opts[:project_count])

if opts[:run_all]
  _, docker_error, docker_status = Open3.capture3("docker login -u root -p #{Config.access_token} #{Config.primary_registry}")

  unless docker_status.success?
    puts docker_error
    exit(1)
  end
end

threads = []

puts "Running Replication Tests for: Images, Snippets and Project Snippets"
threads << Thread.new { ImageReplication.new.start_replication }
threads << Thread.new { SnippetReplication.new.start_replication }
threads << Thread.new { ProjectSnippetReplication.new.start_replication }

threads.each(&:join)
threads = []

puts "Running Replication Tests for: Projects, Issues and Repositories"
threads << Thread.new { ProjectReplication.new.start_replication }
threads << Thread.new { IssueReplication.new.start_replication }
threads << Thread.new { RepositoryReplication.new.start_replication }

threads.each(&:join)
exit(0) unless opts[:run_all]

threads = []

puts "Running Replication Tests for: Container Registry, LFS Objects and Package Registry"
threads << Thread.new { ContainerReplication.new.start_replication }
threads << Thread.new { LFSReplication.new.start_replication }
threads << Thread.new { PackageReplication.new.start_replication }

threads.each(&:join)
