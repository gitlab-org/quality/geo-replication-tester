# Geo Replication Tester

> :information_source:&nbsp;  **WARNING**
> This tool will create new test data and upload to existing projects. At no point should this be run against a production environment.

A testing tool designed to measure the time it takes for a single file to be available on the secondary site after upload to the primary site.

It measures this by uploading a file to the primary site via the `/projects/:id/uploads` endpoint. This then returns a URL which can be used to view the file, on the primary site this will correctly return the file, on the secondary site this will return a 404 until the file has been replicated. The tool will ping the URL on the secondary site every second until a status code of `200 OK` is returned.

## How to use

### Command line options

| Option                     | Description                                                                                      | Default Value |
|----------------------------|--------------------------------------------------------------------------------------------------|---------------|
| `--primary-address`   `-p` | The URL for the primary site.                                                                    | n/a           |
| `--secondary-address` `-s` | The URL for the secondary site.                                                                  | n/a           |
| `--file-path`         `-f` | Absolute path to a file on your system to upload.                                                | n/a           |
| `--api-token`         `-a` | An API token to use for authentication when uploading the file.                                  | n/a           |
| `--run-for-duration`  `-r` | How long should the tool run for, in seconds.                                                    | 60            |
| `--file-upload-wait`  `-i` | How often should the tool upload a new file.                                                     | 6             |
| `--project-id`        `-o` | ID of the project to upload the files to, the `api_token` must have permission for this project. | n/a           |

### Running the tool

You can run the tool by using modified version of the following command:

```shell
./geo-replication-tester.rb -p <Primary site URL> -s <Secondary site URL> -f <Path to file to replicate> -a <API token> -o <Project ID>
```

### Output

The tool doesn't log anything to the command line and will instead place everything into a log file located in `./logs`. The format of the file will look similar to the below. The HTTP status code should always be 200, if the code is 404 this means that the file took more than 5 minutes to replicate, any other code could signify an issue with the deployment.

```shell
Primary Site: <Primary Site URL>
Secondary Site: <Secondary Site URL>
Sending 1 request every 1 second(s) for 5 second(s)

| Uploaded Artifact URL                                                | Uploaded At | Replicated At | Duration | HTTP Status Code |
|----------------------------------------------------------------------|-------------|---------------|----------|------------------|
| /root/artifact/uploads/5553d0e72af6f2cb747f0b4fa32d7008/keyboard.jpg | 10:41:53    | 10:43:0       | 67s      | 200              |
| /root/artifact/uploads/698b005f2c932eae78dee4098f0875a4/keyboard.jpg | 10:41:51    | 10:43:0       | 69s      | 200              |
| /root/artifact/uploads/a3bb5867bcc2c41986db0d28000c1859/keyboard.jpg | 10:41:52    | 10:43:0       | 68s      | 200              |
| /root/artifact/uploads/9dff5fb4d27be5513ca5a74a0235bbba/keyboard.jpg | 10:41:54    | 10:43:0       | 66s      | 200              |
| /root/artifact/uploads/123a8b6097824005382d42ea04b343fa/keyboard.jpg | 10:41:55    | 10:43:0       | 65s      | 200              |

Fastest Replication: 65s
Slowest Replication: 69s
Average Replication: 67s
```
